<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="Personnes")
*/
class Personnes {

  /**
  * @ORM\Column(type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
  * @ORM\Column(type="string", length=100)
  */
  protected $nom;

  /**
  * @ORM\Column(type="string", length=50)
  */
  protected $prenom;

  /**
  * @ORM\Column(type="string", length=100)
  */
  protected $email;

/**
  * @ORM\Column(type="date")
  */
  protected $datededepart;
/**
  * @ORM\Column(type="date")
  */
  protected $datederetour;
/**
  * @ORM\Column(type="time")
  */
  protected $heurededepart;
/**
  * @ORM\Column(type="time")
  */
  protected $heurederetour;
/**
 * @ORM\ManyToOne(targetEntity="ville", inversedBy="ville")
 * @ORM\JoinColumn(name="ville_id", referencedColumnName="id", nullable=false)
 */
  private $ville;
/**
 * @ORM\ManyToOne(targetEntity="moyen", inversedBy="moyen")
 * @ORM\JoinColumn(name="moyen_id", referencedColumnName="id", nullable=false)
 */
  private $moyen;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Personnes
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Personnes
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Personnes
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lies
     *
     * @param string $lies
     *
     * @return Personnes
     */
    public function setLies($lies)
    {
        $this->lies = $lies;

        return $this;
    }

    /**
     * Get lies
     *
     * @return string
     */
    public function getLies()
    {
        return $this->lies;
    }

    /**
     * Set datededepart
     *
     * @param \Date $datededepart
     *
     * @return Personnes
     */
    public function setDatededepart(\Date $datededepart)
    {
        $this->datededepart = $datededepart;

        return $this;
    }

    /**
     * Get datededepart
     *
     * @return \Date
     */
    public function getDatededepart()
    {
        return $this->datededepart;
    }

    /**
     * Set datederetour
     *
     * @param \Date $datederetour
     *
     * @return Personnes
     */
    public function setDatederetour(\Date $datederetour)
    {
        $this->datederetour = $datederetour;

        return $this;
    }

    /**
     * Get datederetour
     *
     * @return \Date
     */
    public function getDatederetour()
    {
        return $this->datederetour;
    }

    /**
     * Set heurededepart
     *
     * @param \Time $heurededepart
     *
     * @return Personnes
     */
    public function setHeurededepart(\Time $heurededepart)
    {
        $this->heurededepart = $heurededepart;

        return $this;
    }

    /**
     * Get heurededepart
     *
     * @return \Time
     */
    public function getHeurededepart()
    {
        return $this->heurededepart;
    }

    /**
     * Set heurederetour
     *
     * @param \Time $heurederetour
     *
     * @return Personnes
     */
    public function setHeurederetour(\Time $heurederetour)
    {
        $this->heurederetour = $heurederetour;

        return $this;
    }

    /**
     * Get heurederetour
     *
     * @return \Time
     */
    public function getHeurederetour()
    {
        return $this->heurederetour;
    }

    /**
     * Set ville
     *
     * @param \AppBundle\Entity\ville $ville
     *
     * @return Personnes
     */
    public function setVille(\AppBundle\Entity\ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \AppBundle\Entity\ville
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set moyen
     *
     * @param \AppBundle\Entity\moyen $moyen
     *
     * @return Personnes
     */
    public function setMoyen(\AppBundle\Entity\moyen $moyen = null)
    {
        $this->moyen = $moyen;

        return $this;
    }

    /**
     * Get moyen
     *
     * @return \AppBundle\Entity\moyen
     */
    public function getMoyen()
    {
        return $this->moyen;
    }
}
